<?php

/**
 * hook_form_alter
 * 
 * Alter OG forms
 */

function og_open_write_form_alter(&$form, &$form_state, $form_id)
{
  $inaccessible_groups = array();
  $node = $form['#node'];

  // Add read only options to group form

  if (og_is_group_type(($node->type))) {
		$form['og_open_write'] = array(
 			'#type' => 'checkbox',
      '#description' => t('Selecting this will allow non-subscribed members to post to this group.'),
      '#title' => t('Set group as global write?'),
      '#weight' => 0,
      '#default_value' => $node->nid ? $node->og_open_write : 0,
    );
  }

  // Alter audience form
  if(og_is_group_post_type($node->type)) {
  	if (!user_access('administer nodes')) {
  	
  		global $user;
  	  
  	  $subs = og_get_subscriptions($user->uid);
  	  $options = array();
		  foreach ($subs as $key => $val) {
		    $options[$key] = $val['title'];
		  }
		  $not_required = og_open_write_options();
			$options = $options + $not_required;
			
  	  $simple = false;
  	  
  	  if (count($options) == 1 && variable_get('og_audience_required', FALSE)) {
		    $gids = array_keys($options);
		    $gid = $gids[0];
		    $groups = array($gid);
		    $simple = true;
		  }
		  elseif (!empty($form_state['og_gids'][0])) {
		  	$simple = !user_access('administer organic groups') && !variable_get('og_audience_checkboxes', TRUE);
		    // populate field from the querystring if sent
		    $groups = explode(',', $form_state['og_gids'][0]);
		    if ($simple) {
		      // filter out any groups where author is not a member. we cannot rely on fapi to do this when in simple mode. 
		      $groups = array_intersect($groups, array_keys($options));
		    }
		  }
		  
		  // Remove old form elements
		  unset($form['og_nodeapi']['visible']['og_groups_visible']);
		  unset($form['og_nodeapi']['visible']['og_groups']);
		  
		  // Emit the audience form element.
	  	if ($simple) {
		    // 'simple' mode. read only.
		    if (count($groups)) {
		      foreach ($groups as $gid) {
		        $titles[] = $options[$gid];
		        $item_value = implode(', ', $titles);
		      }
		      $form['og_nodeapi']['visible']['og_groups_visible'] = array(
		        '#type' => 'item', 
		        '#title' => t('Audience'), 
		        '#value' => $item_value
		      );
		      
		      $assoc_groups = drupal_map_assoc($groups);
		      
		      // this 'value' element persists the audience value during submit process
		      $form['og_nodeapi']['invisible']['og_groups'] = array('#type' => 'value', '#value' => $assoc_groups);
		    }
		  }
		  elseif ($cnt = count($options, COUNT_RECURSIVE)) {
		    // show multi-select. if less than 20 choices, use checkboxes.
		    $type = $cnt >= 20 || $is_optgroup ? 'select' : 'checkboxes';
		    $form['og_nodeapi']['visible']['og_groups'] = array(
		      '#type' => $type, 
		      '#title' => t('Audience'), 
		      '#attributes' => array('class' => 'og-audience'),
		      '#options' => $type == 'checkboxes' ? array_map('filter_xss', $options) : $options, 
		      '#required' => $required, 
		      '#description' =>  format_plural(count($options), 'Show this post in this group.', 'Show this post in these groups.'),
		      '#default_value' => $groups ? $groups : array(),
		      '#required' => $required, 
		      '#multiple' => TRUE);
		  }  	  
	  }
  }
}

  
/**
 *
 * Return an array containing all groups that do not require membership for posts to be made.
 *
 */
 
function og_open_write_options() {
  list($types, $in) = og_get_sql_args();
  $sql = "SELECT n.title, n.nid FROM {node} n INNER JOIN {og} o ON n.nid = o.nid INNER JOIN {og_open_write} g ON n.nid = g.nid WHERE n.type $in AND n.status = 1 AND g.types = 1 ORDER BY n.title ASC";
  $result = db_query($sql, $types);
  while ($row = db_fetch_object($result)) {
    $options[$row->nid] = $row->title;
  }
  return isset($options) ? $options : array();
}

/**
 * hook_node_api
 *
 * Save read only informations
 */

function og_open_write_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch($op) {
  	case 'validate':
  	/**
  	 * TODO: Provide solution for og_access module conflict 
  	  if (og_is_group_type(($node->type))) {
  	  	if($node->og_private && $node->og_open_write) {
	        form_set_error('og_open_write', t("The group cannot be private and have global write access"));
	      }
	    }
	    break;
  	**/
    case 'insert':
    case 'update':
	    // Record whether the group has global write access.
	
	    if (og_is_group_type(($node->type))) {
	      $data = array(
	        'nid' => $node->nid,
	        'types' => $node->og_open_write
	        );
	
	      $result = db_result(db_query('SELECT * FROM {og_open_write} WHERE nid=%d', $node->nid));
	
	      if (!$result) {
	        drupal_write_record('og_open_write', $data);
	      }
	      else {
	        drupal_write_record('og_open_write', $data, 'nid');
	      
	      }
	    }
		  break;
    case 'load':
	    $result = db_query('SELECT * FROM {og_open_write} WHERE nid=%d', $node->nid);
	    $open_write = db_fetch_array($result);
	    return array('og_open_write' => $open_write['write']);
	    break;
    case 'delete':
    	db_query('DELETE FROM {og_open_write} WHERE nid=%d', $node->nid);
    	break;
  }  
}